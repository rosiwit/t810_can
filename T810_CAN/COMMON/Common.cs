﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T810_CAN.COMMON
{
    public class Common
    {
        public static byte[] str2hex(string str)
        {
            string[] bytestr = str.Split(' ');
            byte[] bytes = new byte[bytestr.Length];
            for (int i = 0; i < bytestr.Length; i++)
            {
                bytes[i] = Convert.ToByte(bytestr[i], 16);
            }
            return bytes;
        }

        public static string hex2str(byte[] bytes)
        {
            string[] strs = new string[bytes.Length];
            for (int i = 0; i < bytes.Length; i++)
            {
                strs[i] = bytes[i].ToString("X2");
            }
            return String.Join(" ", strs);
        }

        public static long Get_SystemTime()
        {
            return DateTime.Now.Ticks / 10000;
        }
    }
}
