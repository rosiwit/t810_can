﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T810_CAN.CAN
{
    public class Signal
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public UInt32 CanId { get; set; }
        public int StartBit { get; set; }
        public int BitLength { get; set; }
        public float Factor { get; set; }
        public int Offset { get; set; }
        public float Value { get; set; }
        public void getValueFromFrame(CanFrame frame)
        {
            UInt64 data = BitConverter.ToUInt64(frame.Data);
            Value = ((data >> this.StartBit) & ((UInt64)0x00000001 << this.BitLength - 1)) * this.Factor + this.Offset;
        }

        public static float getSignalFromFrame(CanFrame frame, Signal signal)
        {
            UInt64 data = BitConverter.ToUInt64(frame.Data);
            return ((data >> signal.StartBit) & ((UInt64)0x00000001 << signal.BitLength - 1)) * signal.Factor + signal.Offset;
        }
    }
}
